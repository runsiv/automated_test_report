import os
from pylatex import *
from variables import * # Ikke vakkert men det funker


from pylatex.utils import bold, NoEscape

#set_variable(title, var_json['title'])
class Report(Document):
    def __init__(self):
        super().__init__()
        self.geometry_options = {
        "landscape": False,
        "margin": "1.in",
        "headheight": "25pt",
        "headsep": "20pt",
        "includeheadfoot": True}
        
        self.documentclass = Command(
            'documentclass',
            options=['12pt'],
            arguments=['article']
        )
        
    def gen_header(self):

        header = PageStyle("header")
        # Create left header
        with header.create(Head('L')):
            header.append("Sensorlink Doc. No.")
            header.append(LineBreak())
            header.append("Client Doc. No")
            header.append(LineBreak())
            header.append(Title)
            # create center header
        with header.create(Head('C')):
            header.append(sl_doc_no)
            header.append(LineBreak())
            header.append(client_doc_no)
            header.append(LineBreak())

        doc.preamble.append(header)
        doc.change_document_style("header")


    
    def tableofcontent(self):
        toc = Command('tableofcontents')
        doc.append(toc)
        doc.append(NoEscape(r'\newpage'))


    def add_images(self):
        app1 = Figure(position='h')
        app1.add_image('606910-282-02_02 Sign reduced.pdf')
        doc.append(app1)


    def generate_table(self):
        section = Section("Report Summary")
        report_summary_table = Tabular('|l | l|')
        report_summary_table.add_hline()
        report_summary_table.add_row(["Purpose of Test:", " FAT Temperature Probe Pressure test"])
        report_summary_table.add_hline()
        report_summary_table.add_row(["Test Procedure Title", Test_Procedure])
        report_summary_table.add_hline()
        report_summary_table.add_row(["Test Date", Date])
        report_summary_table.add_hline()
        report_summary_table.add_row(["Personel", Personel])
        report_summary_table.add_hline()
        report_summary_table.add_row(["Location", Location])
        report_summary_table.add_hline()
        report_summary_table.add_row(["Number of units tested", Number_of_units_testet])
        report_summary_table.add_hline()
        report_summary_table.add_row(["Serial Numbers", Serial_Numbers])
        report_summary_table.add_hline()
        report_summary_table.add_row(["Number of units approved", Number_of_Units_approved])
        report_summary_table.add_hline()
        report_summary_table.add_row(["Number of units rejected", Number_of_units_rejected])
        report_summary_table.add_hline()
        report_summary_table.add_row(["Number of punches", Number_of_punches])
        report_summary_table.add_hline()
        section.append(report_summary_table)
        doc.append(section)
        doc.append(NoEscape(r'\newpage'))

    def test_execution(self):
        test_execution_section = Section("Test Execution")
        test_execution_section.append("The procedure was followed as written with no changes ")

        #subsection
        comments_picture = Subsection(" Comments/Picture from Test")

        #append subsection
        test_execution_section.append(comments_picture)
        #NoEscape(r"\renwcommand*includepdf\{606910-282-02\_02 Sign reduced.pdf\}")
        
        doc.append(test_execution_section)

    # added latex package
    def add_packages(self):
        self.preamble.append(Package('pdfpages'))
        self.preamble.append(Package('setspace'))
        self.preamble.append(Package('bookmark'))
        self.preamble.append(NoEscape(r'\doublespacing'))
    
    def add_attachment(self):
        #self.preamble.append(Command(arguments=NoEscape(r'\begin{figure}(0,0) \put(0,0){\includegraphics[]{asd}} \end{figure}')))
        section = Section("Attachments")
        doc.append(section)
        section = Section("Checklist")
        doc.append(section)
        doc.append(NoEscape(r'\includepdf[pages=-]{test}'))
        section = Section("Climate chamber")
        doc.append(section)
    

if __name__ == "__main__":    
    doc = Report()
    doc.gen_header()
    doc.add_packages()
    doc.tableofcontent()
    doc.generate_table()
    doc.test_execution()
    #doc.add_images()
    doc.add_attachment()
    doc.generate_pdf(Title)
    doc.generate_tex(Title)