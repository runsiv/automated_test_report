import os

from pylatex import Document, PageStyle, Head, Foot, MiniPage, \
    StandAloneGraphic, MultiColumn, Tabu, LongTabu, LargeText, MediumText, \
    LineBreak, NewPage, Tabularx, TextColor, simple_page_number
from pylatex.utils import bold, NoEscape

def generate_tabus(dt, SN):
    geometry_options  = {
        "landscape": True,
        "margin": "1.5in",
        "headheight": "20pt",
        "headsep": "12pt",
        "includeheadfoot": True
    }
    report = Document(page_numbers=True, geometry_options =  geometry_options )

    # Generate data table with tight columns
    fmt = "X[r] X[r]"
    with report.create(LongTabu(fmt, spread="0pt")) as data_table:
        data_table.add_hline()
        data_table.add_row(["Purpose of Test:", " FAT Temperature Probe Pressure test"])
        data_table.add_hline()
        data_table.add_row(["Test Procedure Title", "Temperature Probe - FAT Pressure Test"])
        data_table.add_hline()
        data_table.add_row(["Test Data", dt])
        data_table.add_hline()
        data_table.add_row(["Serial Numbers", SN])
        data_table.add_hline()
    
    report.generate_pdf()
    report.generate_tex()
dt = "2019-06-30"
SN = "2019-09999"

generate_tabus(dt, SN)


